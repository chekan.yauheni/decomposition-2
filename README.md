
# Decomposition-2

# Task 1

## Solve the tasks:

- Write a method(s) that checks whether the given three numbers are coprime.


- Implement the addition of two numbers of unlimited length. Use the String type to store numbers. 
To solve the problem, use the division of code into methods.


- A natural number with n digits is called an Armstrong number if the sum of its digits raised to the power of n is 
equal to the number itself. Find all Armstrong numbers from 1 to k. 
To solve the problem, use the division of code into methods.
For example, `371` is an Armstrong number because `3^3 + 7^3 + 1^3 = 371`.


- An array D is given. Determine the following sums: `D[1] + D[2] + D[3]; D[3] + D[4] + D[5]; D[4] +D[5] +D[6]`.
Explanation. Compose a method(s) for calculating the sum of three consecutive array elements with numbers from k to m.


- Super Lock». The secret lock for the safe consists of 10 cells arranged in a row, into which you need to insert dice. 
But the door opens only if in any three neighboring cells the sum of points on the front faces of the dice is 10. 
(A dice has from 1 to 6 points on each face). Write a program that solves the code of the lock, provided that two dice 
are already inserted into the cells. To solve the problem, use the division of code into methods.