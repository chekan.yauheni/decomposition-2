package com.epam.rd;

import java.util.Arrays;

public class ArmstrongNumber {
    public static void main(String[] args) {
        int[] armstrNums = findArmstrongNums(3729);
        System.out.println(Arrays.toString(armstrNums));
    }

    public static int[] findArmstrongNums(int range) {
        int[] armstrongNums = new int[range];
        int index = 0;
        for (int i = 1; i <= range; i++) {
            if (checkIsArmstrong(i)) {
                armstrongNums[index] = i;
                index++;
            }
        } return refineArray(armstrongNums);
    }

    public static boolean checkIsArmstrong(int number) {
        char[] splitNum = String.valueOf(number).toCharArray();
        // Offset is the index at which numbers start in ASCII table
        int offset = 48;
        int sumOfDigits = 0;
        for (char c : splitNum) {
            int digit = (int) c - offset;
            sumOfDigits += Math.pow(digit, splitNum.length);
        }
        return number == sumOfDigits;
    }

    public static int[] refineArray(int[] arrayToRefine) {
        int newArrayLen = 0;
        for(int number: arrayToRefine) {
            if (number > 0) {
                newArrayLen++;
            }
        }
        return Arrays.copyOf(arrayToRefine, newArrayLen);
    }
}
