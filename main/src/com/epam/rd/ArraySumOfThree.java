package com.epam.rd;

import java.util.Arrays;
import java.util.Random;

public class ArraySumOfThree {

    public static void main(String[] args) {
        int[] arrayOfNumbers = generateArray(12, 10);
        int sumElements = getSumElements(arrayOfNumbers, 0, 3);
        int sumElementsByIndex = getSumElementsByIndex(arrayOfNumbers, 3, 8);

        System.out.println(Arrays.toString(arrayOfNumbers));
        System.out.printf("Sum of first 3 elements: %s\n", sumElements);
        System.out.printf("Sum of elements between index 3 and 8: %s\n", sumElementsByIndex);
    }

    public static int[] generateArray(int arrayLen, int numRange) {
        Random random = new Random();
        int[] randomArray = new int[arrayLen];
        for (int i = 0; i < randomArray.length; i++) {
            randomArray[i] = random.nextInt(numRange);
        } return randomArray;
    }

    public static int getSumElements(int[] array, int startIndex, int numElements) {
        int endIndex = startIndex + numElements;
        int sum = 0;
        for(int i = startIndex; i < endIndex; i++) {
            sum += array[i];
        } return sum;
    }

    public static int getSumElementsByIndex(int[] array, int startIndex, int stopIndex) {
        int numberElements = stopIndex - startIndex;
        return getSumElements(array, startIndex, numberElements);
    }
}
