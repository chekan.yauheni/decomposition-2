package com.epam.rd;


public class Coprime {
    public static void main(String[] args) {
        int a = 9;
        int b = 8;
        int c = 17;

        int d = 16;
        int e = 22;
        int f = 37;

        System.out.printf("Numbers %s, %s, %s are coprime: %s\n", a, b, c, checkCoprimeThree(a, b, c));
        System.out.printf("Numbers %s, %s, %s are coprime: %s\n", d, e, f, checkCoprimeThree(d, e, f));
    }

    public static boolean checkCoprimeThree(int a, int b, int c) {
        return checkCoprimeTwo(a, b) && checkCoprimeTwo(b, c) && checkCoprimeTwo(c, a);
    }

    public static int[] getNumberFactors(int number) {
        int[] numFactors = new int[number];
        int arrayIndex = 0;
        int divisor = 1;
        while (divisor <= number) {
            if (number % divisor == 0) {
                numFactors[arrayIndex] = divisor;
                arrayIndex++;
            }
            divisor++;
        }
        return numFactors;
    }

    public static boolean checkCoprimeTwo(int a, int b) {
        int commonFactorsNumber = 0;
        int[] aFactors = getNumberFactors(a);
        int[] bFactors = getNumberFactors(b);
        for (int i = 0; aFactors[i] != 0; i++) {
            for (int j = 0; bFactors[j] != 0; j++) {
                if (aFactors[i] == bFactors[j]) {
                    commonFactorsNumber++;
                }
            }
        }
        return commonFactorsNumber == 1;
    }
}