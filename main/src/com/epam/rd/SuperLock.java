package com.epam.rd;

import java.util.Arrays;
import java.util.Random;

public class SuperLock {

    public static void main(String[] args) {
        int[] lockCells = new int[10];
        insertFirstDices(lockCells);

        System.out.printf("Initial lock cells: %s\n", Arrays.toString(lockCells));
        openLock(lockCells);

        System.out.printf("Open lock combination: %s\n", Arrays.toString(lockCells));
    }

    public static void insertFirstDices(int[] lockCells) {
        Random rnd = new Random();
        int firstDice = rnd.nextInt(1, 7);
        int secondDice = rnd.nextInt(1, 7);
        while (firstDice + secondDice < 4 || firstDice + secondDice > 9) {
            firstDice = rnd.nextInt(1, 7);
            secondDice = rnd.nextInt(1, 7);
        }
        lockCells[0] = firstDice;
        lockCells[1] = secondDice;
    }

    public static void openLock(int[] lockCells) {
        for (int i = 2; i < lockCells.length; i++) {
            int selectedDice = getNextDice(i, lockCells);
            lockCells[i] = selectedDice;
        }
    }

    public static int getNextDice(int index, int[] lockCells) {
        return 10 - (lockCells[index - 1] + lockCells[index - 2]);
    }
}
