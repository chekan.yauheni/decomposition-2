package com.epam.rd;

import java.util.Arrays;

public class UnlimitedAddition {
    public static void main(String[] args) {
        String aStr = "999999999";
        String bStr = "9000000001";

        String result = add(aStr, bStr);
        System.out.println(result);
    }

    public static String add(String a, String b) {
        // Convert the strings to char arrays
        char[] aChars = a.toCharArray();
        char[] bChars = b.toCharArray();

        // Determine the length of the result by taking the maximum length of the two input strings
        int resultLength = Math.max(aChars.length, bChars.length) + 1;
        char[] resultChars = new char[resultLength];

        // Initialize the carry variable to 0
        int carry = 0;

        // Offset is the index at which numbers start in ASCII table
        int digitOffset = 48;

        // Iterate over the char arrays from right to left
        for (int i = resultLength - 1; i >= 0; i--) {
            // Python style negative index
            int reverseIdx = i - resultLength;
            // Get the current digits from the input strings, or 0 if the digit does not exist
            int firstDigit = toDigit(reverseIdx, aChars, digitOffset);
            int secondDigit = toDigit(reverseIdx, bChars, digitOffset);

            // Add the digits and the carry, and store the result in the result array
            int sum = firstDigit + secondDigit + carry;
            carry = (sum > 9) ? 1 : 0;
            if (i == 0 && sum == 0) {
                // Avoid appending leading zero
                resultChars = Arrays.copyOfRange(resultChars, i + 1, resultLength);
            } else resultChars[i] = (char)(sum % 10 + digitOffset);
        }

        // Convert the result char array to a String and return it
        return new String(resultChars);
    }

    public static int toDigit(int reverseIdx, char[] currentArray, int offset) {
        int currentDigit = 0;
        int indexInCurrentArray = reverseIdx + currentArray.length;
        // If index is not out of array get the digit, otherwise return 0
        if (indexInCurrentArray >= 0) {
            int charAsciiNum = currentArray[indexInCurrentArray];
            currentDigit = charAsciiNum - offset;
        } return currentDigit;
    }
}
